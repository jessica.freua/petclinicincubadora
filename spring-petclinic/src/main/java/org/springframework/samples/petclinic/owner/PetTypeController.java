package org.springframework.samples.petclinic.owner;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PetTypeController {
	
	@GetMapping("/pettypes")
	public String showPetTypeList(Model model) {
		// TODO: consultar banco de dados!
		//FIXME: Mock up list!
		ArrayList<PetType> pets = new ArrayList<PetType>();
		String[] petNames = {"cat", "dog", "lizard", "snake", "bird", "hamster"};
		for (String n : petNames) {
			PetType p = new PetType();
			p.setName(n);
			pets.add(p);
		}
		model.addAttribute("pets", pets);
		return "pets/petTypeList";
	}

}
